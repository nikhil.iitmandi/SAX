SAX tranformation of time series data using python
I have made a simple gui using Tkinter library

You can use this file to learn about making subplots in matplotlib, making simple gui using Tkinter.

# Important points to run to code:

1-Code will run with python 2.

2-Libraries Required for the code:

Tkinter

ttk

numpy

pandas

pylab

matplotlib

scipy

3- Open the terminal at the location of the code and run
```shell
$ python sax.py
```
# Usage of GUI:
![GUI](gui.png)


GUI takes 3 inputs.

1- Length: Takes ineteger value. User can specify the length of the time series data that will be
generated to apply SAX transformation

2- Splits: Takes integer value. Number of splits to perform PAA.

3- Alphabets: Takes string. This sepcifies the number of alphabets that will be used for SAX
representation. Example : ABCDEF.

# Results
![ACCURACY](SAX_Plots.png)