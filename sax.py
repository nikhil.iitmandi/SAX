from Tkinter import *
import ttk
import numpy as np
import pandas as pd
import pylab as pl
import matplotlib.pyplot as plt
from scipy.stats import norm

class Application(Frame):
    
    def __init__(self, master=None):
        Frame.__init__(self, master)
        #self.pack()
        self.createWidgets()

    def createWidgets(self):

        self.length = Label(text='Length', relief=RIDGE,width=15).grid(row=0,column=0)
        self.lengthVal = Entry(bg='white', relief=SUNKEN,width=10)
        self.lengthVal.grid(row=0,column=1)
        self.lengthVal.insert(END,'100')

        self.splits = Label(text='Splits', relief=RIDGE,width=15).grid(row=1,column=0)
        self.splitVal = Entry(bg='white', relief=SUNKEN,width=10)
        self.splitVal.grid(row=1,column=1)
        self.splitVal.insert(END,'5')

        self.alphabets = Label(text='Alphabets', relief=RIDGE,width=15).grid(row=2,column=0)
        self.alphabetVal = Entry(bg='white', relief=SUNKEN,width=10)
        self.alphabetVal.grid(row=2,column=1)
        self.alphabetVal.insert(END,'abcd')
        
        self.quit = Button(text= 'QUIT',fg = 'red',command = self.quit).grid(row=4,column=0)
        self.run = Button(text= 'RUN',fg = 'green',command = self.run).grid(row=4,column=1)
        #self.QUIT["text"] = "QUIT"
        #self.QUIT["fg"]   = "red"
        #self.QUIT["command"] =  self.quit
    
    def generate(self,length):
        
        ts = pd.Series(np.random.randn(length), index=pd.date_range('1/1/2000', periods=length))
        ts = ts.cumsum()
        return ts

    def normalise(self,ts):
        
        mus = ts.mean(axis = 0)
        stds = ts.std(axis = 0)
        return (ts - mus) / stds
    
    def paa_transform(self,ts, n_pieces):
        #split along columns    
        splitted = np.array_split(ts, n_pieces) 
        return np.asarray(map(lambda xs: xs.mean(axis = 0), splitted))
    
    def sax_transform(self,ts, n_pieces, alphabet):
        
        numAlpha = len(alphabet)
        self.thres = norm.ppf(np.linspace(1./numAlpha,
                                        1-1./numAlpha,
                                        numAlpha-1))
        
        paa_ts = self.paa_transform(self.normalise(ts), n_pieces)
        return np.apply_along_axis(self.translate, 0, paa_ts)

    def translate(self,ts_values):
            return np.asarray([(self.alphabet[0] if ts_value < self.thres[0]
                    else (self.alphabet[-1] if ts_value > self.thres[-1]
                          else self.alphabet[np.where(self.thres <= ts_value)[0][-1]+1]))
                               for ts_value in ts_values])
    def run(self):
        plt.clf()
        print "================================"

        ########################################
        ## Take these args from user ###########
        ########################################
        numSplits = int(self.splitVal.get())
        length = int(self.lengthVal.get())
        self.alphabet = self.alphabetVal.get()
        ########################################

        ########################################
        ######### Generate TS data #############
        ########################################
        data1 = self.generate(length)
        data2 = self.generate(length)
        ts = pd.DataFrame({"data1": data1, "data2": data2})
        ########################################

        ########################################
        ####### Normalizing Data ###############
        ########################################
        zts = self.normalise(ts)
        ########################################

        ########################################
        #### PAA Transformed ###################
        ########################################
        split = self.paa_transform(zts, numSplits)
        ########################################

        ########################################
        #### Transoform to SAX #################
        ########################################
        numAlpha = len(self.alphabet)
        self.thres = norm.ppf(np.linspace(1./numAlpha,
                                            1-1./numAlpha,
                                            numAlpha-1))

        SAX =  np.apply_along_axis(self.translate, 0, split)
        #output = self.sax_transform(ts, 9, "abcd")
        print ("SAX represenation of the two output is:")
        print (SAX)
        print ("You can change paramters and RUN again")
        #########################################

        #########################################
        ######Data Visualization ####################
        #########################################
        x = pd.date_range('1/1/2000', periods=length)
        start = x[0]
        end = x[length-1]
        x = np.linspace(start.value, end.value, length)
        x = pd.to_datetime(x)
        
        font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 15}

        plt.rcParams.update({'font.size': 12})
        fig = plt.figure(1)
        
        tsData = plt.subplot2grid((3,2), (0,0), rowspan=1, colspan=1)
        tsData.plot(x, data1, '-+', label='data-1')
        tsData.plot(x, data2, '-+', label='data-2')
        plt.title('Time Series Data')
        tsData.legend()

        normData = plt.subplot2grid((3,2), (0,1), rowspan=1, colspan=1)
        normData.plot(x, zts.iloc[:, 0], '-+', label='data-1-norm')
        normData.plot(x, zts.iloc[:, 1], '-+', label='data-2-norm')
        normData.axhline(0.0, color='r', linestyle='--', lw=1)
        plt.title('Time Series Data Normalised')
        normData.legend()

        sl = np.linspace(start.value, end.value, numSplits+1)
        sl = pd.to_datetime(sl)
        
        
        ppa1 = plt.subplot2grid((3,2), (1,0), rowspan=1, colspan=1)
        ppa1.plot(x, zts.iloc[:, 0], '-+', label='data-1-norm')
        for i in range(1,numSplits):
            ppa1.plot([sl[i-1], sl[i]], [split[i-1][0], split[i-1][0]], linewidth=2, linestyle="-", c="green",solid_capstyle="projecting")
            ppa1.axvline(sl[i], color='r', linestyle='--', lw=2)
        ppa1.plot([sl[numSplits-1], sl[numSplits]], [split[numSplits-1][0], split[numSplits-1][0]], linewidth=2, linestyle="-", c="green",solid_capstyle="projecting",label='data-1-PAA')
        ppa1.set_title('Data1 and its PAA')
        ppa1.legend()

        ppa2 = plt.subplot2grid((3,2), (1,1), rowspan=1, colspan=1)
        ppa2.plot(x, zts.iloc[:, 1], '-+', label='data-2-norm')
        for i in range(1,numSplits):
            ppa2.plot([sl[i-1], sl[i]], [split[i-1][1], split[i-1][1]], linewidth=2, linestyle="-", c="green",solid_capstyle="projecting")
            ppa2.axvline(sl[i], color='r', linestyle='--', lw=2)
        ppa2.plot([sl[numSplits-1], sl[numSplits]], [split[numSplits-1][1], split[numSplits-1][1]], linewidth=2, linestyle="-", c="green",solid_capstyle="projecting",label='data-2-PAA')
        ppa2.set_title('Data2 and its PAA')
        ppa2.legend()

        saxed = plt.subplot2grid((3,2), (2,0), rowspan=1, colspan=2)
        for i in range(1,numSplits):
            saxed.plot([sl[i-1], sl[i]], [split[i-1][0], split[i-1][0]], linewidth=2, linestyle="-", c="blue",solid_capstyle="projecting")
            saxed.plot([sl[i-1], sl[i]], [split[i-1][1], split[i-1][1]], linewidth=2, linestyle="-", c="green",solid_capstyle="projecting")
            saxed.axvline(sl[i], color='r', linestyle='--', lw=2)
        saxed.plot([sl[numSplits-1], sl[numSplits]], [split[numSplits-1][0], split[numSplits-1][0]], linewidth=2, linestyle="-", c="blue",solid_capstyle="projecting",label='data-1-PAA')
        saxed.plot([sl[numSplits-1], sl[numSplits]], [split[numSplits-1][1], split[numSplits-1][1]], linewidth=2, linestyle="-", c="green",solid_capstyle="projecting",label='data-2-PAA')
        for i in range(numAlpha - 1):
            saxed.axhline(self.thres[i], color='black', linestyle='-', lw=0.5)
        saxed.set_title('SAX')
        saxed.legend()
        #plt.tight_layout()
        fig.set_size_inches(30, 20)
        plt.savefig('SAX_Plots.png')
        
        mng = plt.get_current_fig_manager()
        mng.resize(*mng.window.maxsize())
        plt.show()
        

root = Tk()
app = Application(master=root)
app.mainloop()
root.destroy()
